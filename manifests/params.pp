class django::params {
  require postgresql::server
  $webroot       = '/var/www'
  $gunicorn_user = 'nginx'
}
